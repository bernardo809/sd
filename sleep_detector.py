import cv2
import numpy as np
import os, shutil, re
from os.path import isfile, join
import os
import pathlib
import base64
from pathlib import Path
import tempfile

from scipy.spatial import distance as dist
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import imutils
import time
import dlib
import time
from flask_socketio import emit
#from eye_aspect_ratio import eye_aspect_ratio


class GenerateVideoClass:

	def __init__(self, Session):
		self.images = Session
		self.directory = tempfile.mkdtemp()
		

	def convertToVideo(self):
		images = self.images
		frame_array =  []
		i = 0
		#Recorremos el arreglo de imagenes para pasarlas de Base64 a Mapa de Bytes
		for v in images:
			v = v.replace("data:image/png;base64,", "")
			h = base64.b64decode(v)
			i = i+1
			#crear un archivo de imagen y guarda en ella el base64 decodificado
			image_result = open(self.directory+'/'+str(i)+".jpg", 'wb')
			image_result.write(h)

			#Ordena los archivos
			files = [f for f in os.listdir(self.directory) if isfile(join(self.directory, f))]
			r = re.compile(r"(\d+)")  
			files.sort(key=lambda x: int(r.search(x).group(1)))

		#For para recorrer el array de archivos y crear el video	
		for i in range(len(files)):
			filename = self.directory+"/"+files[i]
			img=cv2.imread(filename)
			height, width, layers = img.shape
			size = (width,height)
			for k in range (10):
				frame_array.append(img)
		images = []
		#Se crea el archivo del video con un nombre aleatorio
		video_name = tempfile.mkstemp(prefix='vid_')
		
		out = cv2.VideoWriter(video_name[1]+'.mp4',cv2.VideoWriter_fourcc(*'mp4v'), 25, size)
		
		for i in range(len(frame_array)):
			out.write(frame_array[i])
		
		out.release()
		#video_name 
		name_v = str(video_name[1])+".mp4"
		
		#Si el archivo de video existe se llama a la funcion para detectar el sueño
		sleep = False
		if os.path.isfile(name_v):
			print("El fichero existe")
			self.sleepDetector(video_name)
		else:
			print("El fichero no existe")

		#BORRA EL CONTENIDO DE LA CARPETA DONDE SE GUARDARON LOS FRAMES
		#shutil.rmtree(self.directory)
		for f in os.listdir(self.directory):
			file_path = os.path.join(self.directory, f)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
			except Exception as e:
				print(e)

	def sleepDetector(self, video):
		COUNTER = 0
		TOTAL = 0
		sleep=0
		gradsleep=0
		cap = cv2.VideoCapture(str(video[1])+".mp4")
		fileStream = False
		time.sleep(1.0)
		#print(cv2.COLOR_BGR2GRAY, frame)
		mStart = 48
		mEnd = 68
		jStart = 0
		jEnd = 17
		rlStart = 17
		rlEnd = 22
		leStart = 22
		leEnd = 27
		nStart = 27
		nEnd = 36
		EYE_AR_THRESH = 0.3
		EYE_AR_CONSEC_FRAMES = 2
		sleep1_FRAMES = 3*5
		sleep2_FRAMES = 7*5
		sleep3_FRAMES = 8*5
		sleep4_FRAMES = 10*5
		sleep01_FRAMES = 6
		sleep00_FRAMES = 2

		(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
		(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
		(mStart, mEnd) = face_utils.FACIAL_LANDMARKS_IDXS["mouth"]
		(jStart, jEnd) = face_utils.FACIAL_LANDMARKS_IDXS["jaw"]
		(reStart, reEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eyebrow"]
		(leStart, leEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eyebrow"]
		(nStart, nEnd) = face_utils.FACIAL_LANDMARKS_IDXS["nose"]

		detector = dlib.get_frontal_face_detector()
		PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"
		directory =  os.getcwd()+'/app/video/'
		predictor = dlib.shape_predictor(directory+PREDICTOR_PATH)
		
		while (cap.isOpened()):
			try:
				ret, frame = cap.read()	
				gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
				rects = detector(gray, 0)
				for rect in rects:
					shape = predictor(gray, rect)
					shape = face_utils.shape_to_np(shape)
					leftEye = shape[lStart:lEnd]
					rightEye = shape[rStart:rEnd]
					mouth = shape[mStart:mEnd]
					jaw = shape[jStart:jEnd]
					re = shape[reStart:reEnd]
					le = shape[leStart:leEnd]
					nose = shape[nStart:nEnd]
					leftEAR = self.eye_aspect_ratio(leftEye)
					rightEAR = self.eye_aspect_ratio(rightEye)

					ear = (leftEAR + rightEAR) / 2.0
					leftEyeHull = cv2.convexHull(leftEye)
					mouthHull = cv2.convexHull(mouth)
					jawHull = cv2.convexHull(jaw)
					reHull = cv2.convexHull(re)
					leHull = cv2.convexHull(le)
					noseHull = cv2.convexHull(nose)
					rightEyeHull = cv2.convexHull(rightEye)
					cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
					cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

					if ear < EYE_AR_THRESH:
						COUNTER += 1
					else:
						if COUNTER >= EYE_AR_CONSEC_FRAMES:
							TOTAL += 1
						if COUNTER >= sleep00_FRAMES:
							gradsleep = 0.2
						if COUNTER >= sleep01_FRAMES:
							gradsleep = 0.5
						if COUNTER >= sleep1_FRAMES:
							gradsleep = 1
						if COUNTER >= sleep2_FRAMES:
							gradsleep = 2
						if COUNTER >= sleep3_FRAMES:
							gradsleep = 4
						if COUNTER >= sleep4_FRAMES:
							gradsleep = 10
						
						# reseteo del contador
						COUNTER = 0

				if gradsleep >= 4:
					print("True "+str(COUNTER))
					emit('response', True)
				else:
					print("False"+str(COUNTER))
					emit('response', False)
			except:
				cap.release()

			#cv2.imshow("Frame", frame)
			#key = cv2.waitKey(1) & 0xFF		

		cap.release()
		cv2.destroyAllWindows()
		shutil.rmtree(self.directory)
		#shutil.rmtree(video[1])

	def eye_aspect_ratio(self, eye):
		# Calcula las distancias euclidianas entre los dos conjuntos de
		# Señales verticales del ojo (x, y) -coordenadas
			A = dist.euclidean(eye[1], eye[5])
			B = dist.euclidean(eye[2], eye[4])
		 
		# Calcular la distancia euclidiana entre la horizontal
			C = dist.euclidean(eye[0], eye[3])
		 
		# calcula AR del ojo
			ear = (A + B) / (2.0 * C)
		 
		# devuelve AR del ojo
			return ear
