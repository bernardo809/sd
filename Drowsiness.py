from scipy.spatial import distance as dist
from imutils.video import VideoStream
from imutils import face_utils
from threading import Thread
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2
import os

def eye_aspect_ratio(eye):
    	# compute the euclidean distances between the two sets of
	# vertical eye landmarks (x, y)-coordinates
	A = dist.euclidean(eye[1], eye[5])
	B = dist.euclidean(eye[2], eye[4])
	# compute the euclidean distance between the horizontal
	# eye landmark (x, y)-coordinates
	C = dist.euclidean(eye[0], eye[3])
	# compute the eye aspect ratio
	ear = (A + B) / (2.0 * C)
	# return the eye aspect ratio
	return ear


def analize(frame):
    EYE_AR_THRESH = 0.3
    EYE_AR_CONSEC_FRAMES = 48
    # initialize the frame counter as well as a boolean used to
    # indicate if the alarm is going off
    COUNTER = 0
    ALARM_ON = False
    
    detector = dlib.get_frontal_face_detector()
    
    PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"
    directory =  os.getcwd()+'/'
    
    predictor = dlib.shape_predictor(directory+PREDICTOR_PATH)
    (lStart, lEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]   
    (rStart, rEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]

    frame = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
 
    rects = detector(gray, 0)
    for rect in rects:
    		# determine the facial landmarks for the face region, then
		# convert the facial landmark (x, y)-coordinates to a NumPy
		# array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
		# extract the left and right eye coordinates, then use the
		# coordinates to compute the eye aspect ratio for both eyes
        leftEye = shape[lStart:lEnd]   
        rightEye = shape[rStart:rEnd]
        leftEAR = eye_aspect_ratio(leftEye)
        rightEAR = eye_aspect_ratio(rightEye)
		# average the eye aspect ratio together for both eyes
        ear = (leftEAR + rightEAR) / 2.0
  
        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)
        cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)
        if ear < EYE_AR_THRESH:
            COUNTER += 1
			# if the eyes were closed for a sufficient number of
			# then sound the alarm
            if COUNTER >= EYE_AR_CONSEC_FRAMES:
				# if the alarm is not on, turn it on
                if not ALARM_ON:
                    return True
					# check to see if an alarm file was supplied,
					# and if so, start a thread to have the alarm
					# sound played in the background
				# draw an alarm on the frame
		# otherwise, the eye aspect ratio is not below the blink
		# threshold, so reset the counter and alarm
        else:
            COUNTER = 0
            return False
        
              
        