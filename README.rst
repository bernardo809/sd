Audio, video and data channel server
====================================

This example illustrates establishing audio, video and a data channel with a
browser. It also performs some image processing on the video frames using
OpenCV.

Running
-------

Create venv
pip install -r requeriments.txt
python server.py

